const iconOn = document.querySelector('.icon-password')
const password = document.querySelector('.password')
const i = document.querySelectorAll('.changeIcon')

const changeType = (pas, elem) => {
    if (pas.type === 'password') {
        return pas.type = 'text',
            elem.classList.remove('fa-eye'),
            elem.classList.add('fa-eye-slash')
    } else {
        return pas.type = 'password',
            elem.classList.remove('fa-eye-slash'),
            elem.classList.add('fa-eye')
    }
}

iconOn.addEventListener('click', () => {
    changeType(password, i[0])
})

const iconOnSecond = document.querySelector('.icon-password-confirm')
const passwordSecond = document.querySelector('.confirm')
iconOnSecond.addEventListener('click', () => {
    changeType(passwordSecond, i[1])
})

const form = document.querySelector('.password-form')
form.addEventListener('submit', (event) => {
    event.preventDefault();

    if (password.value === passwordSecond.value) {
        return alert('You are welcome')
    } else {
        const error = document.createElement('span')
        error.innerText = 'Потрібно ввести однакові значення'
        error.style.color = 'red';
        error.style.position = 'relative';
        error.style.left = '0%';
        error.style.top = "-20px";
        error.style.fontWeight = '700';

        const errorFather = document.querySelector('.second-wrapper')
        errorFather.appendChild(error)
    }
})